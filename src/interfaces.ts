export interface YelpDataReview{
    id: string,
    rating: number,
    text:string,
    time_created: string,
    url: string
    user:{
        id: string,
        image_url: string,
        name:string,
        profile_url:string,
    }
}
export interface Words{
    length: number;
    words:string;
}
export interface YelpBusinessData{
    id: string
    alias:string;
    categories:[{
        alias:string;
        title:string;
    }]
    coordinates:{
        latitude:number;
        longitude:number;
    }
    display_phone:string;
    distance:number;
    image_url:string;
    is_close:boolean;
    location:{
        address1:string;
        address2:string;
        address3:string;
        city: string;
        country:string;
        display_address?:[]
        state:string;
        zip_code:string;
    }
    name:string;
    phone:string;
    price:string;
    rating:number;
    review_count:number;
    transaction?:[]
    url:string;
}
export interface Ids{
    word_id:number;
    id_business:number;
}
export interface WordsAndBusinesId{
    words:Array<string>;
    id_busines:number;
}
export interface businessesIds{
    id:number;
    id_business:number;
}
export interface CityInterface{
    id:number;
    city_name:string;
}
export interface CityRadius{
    city:string;
    radius?:number
}
export interface BusinesssData{
    address:string;
    alias:string;
    cities_id:number;
    country:string;
    display_address:string;
    id:number;
    name:string;
    rating:number;
    review_count:number;
    yelp_business_id:string;
    zip_code:number;
 }
 export interface Keywords{
    keywords_id:number;
    numberOfBusinesses?:number;
 }
 export interface WordsAndBusinessId{
    length: number;
    id:number;
    businesses_id:number;
    keywords_id:number;
    word:{
        words:string
    };
 }
 export interface BusinessesId{
    businesses_id:number;
 }
 export interface WordAndBusinesId{
    word:string;
    numberOfBusinesses:[{
        businesses_id:number;
    }]
 }
 export interface BusinesId{
    id:number
 }
 export interface KeywordsAndBusinessId{
    numberOfBusinesses?:number;
    keywords_id:number;
    businesses_id:number;
 }
 export interface FinalData{
    saveCity:[{
        city:string;
        words:string;
    }]
 }