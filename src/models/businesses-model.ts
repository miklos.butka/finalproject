import { QueryTypes, Sequelize } from "sequelize";
import { BusinesId, BusinesssData, CityInterface } from "../interfaces";
export class BusinessModel{
    sequelize : any;
    constructor(){
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async insert(businessesInformation:string){
        let insertToBusinessTable = `INSERT INTO businesses(cities_id, yelp_business_id,alias, name, review_count, rating, address, zip_code, country, display_address) VALUES ${businessesInformation}`;
        await this.sequelize.query(insertToBusinessTable);
    }
    async selectByCityId(city:CityInterface):Promise<BusinesId[] | []> {
        let data = `SELECT id FROM businesses WHERE cities_id='${city.id}'`;
        let businessId = await this.sequelize.query(data, { type: QueryTypes.SELECT });
            if (businessId.length < 1) {
        throw new Error(`You don't have any businesses inserted for this city:${city.city_name}.Please update you're businesses table.`)
            }
        return businessId;
    }
    async selectByYelpBusinessId(yelpBusinessId:string):Promise<BusinesssData[]| []> {
        let data = `SELECT * FROM businesses WHERE yelp_business_id='${yelpBusinessId}'`;
        let dataFromBusinesses = await this.sequelize.query(data, { type: QueryTypes.SELECT });
        return dataFromBusinesses;
    }
}