import { QueryTypes, Sequelize } from "sequelize";
export class Keywords{
    sequelize:any;
    constructor() {
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async select(word: string):Promise<[] | number[]>{
        let verifyId = `SELECT id FROM keywords WHERE words="${word}"`
        let idFromKeyword = await this.sequelize.query(verifyId, { type: QueryTypes.SELECT })
        return idFromKeyword;
    }
    async insert(finalWord: string):Promise<any[]>{
        let insertToKeywordsTable = `INSERT INTO keywords(words) VALUES ${finalWord}`;
        let insertedWord = await this.sequelize.query(insertToKeywordsTable);
        return insertedWord;
    }
}