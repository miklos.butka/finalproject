import { businessesIds, CityInterface, FinalData, Ids, Keywords, WordAndBusinesId, WordsAndBusinesId } from "../interfaces";
import { BusinessesKeywordsModel } from "../models/businesses-keywords-model";
import { BusinessModel } from "../models/businesses-model";
import { CityModel } from "../models/cities-model";
import { Yelp } from "../yelp-api/yelp";
import { Review } from "./review";
export class BusinessKeyword {
    constructor() { }
    async businessKeyword(ids: Ids[]) {
        let insert = new BusinessesKeywordsModel()
        for (let objectOfIds of ids) {
            let wordAndBusinessId = await insert.selectByWordIdAndBusinessID(objectOfIds);
            if (typeof objectOfIds.word_id == 'number' && objectOfIds.word_id > 0 && typeof objectOfIds.id_business == 'number' && objectOfIds.id_business > 0) {
                if (wordAndBusinessId.length < 1) {
                    await insert.insertBusinesKeywords(objectOfIds)
                }
            }
        }
    }
    async yelpForBusiness(business: businessesIds[]): Promise<WordsAndBusinesId[] | []> {
        let response: Array<WordsAndBusinesId> = [];
        for (let i = 0; i < business.length; i++) {
            let yelpApi = new Yelp()
            let reviews = await yelpApi.reviews(business[i])
            if(reviews == null){
                throw new Error("Data from YELP unavaible.")
            }
            let filtrationOfReviews = new Review();
            let wordsValidation: string[] = await filtrationOfReviews.filterKeywords(reviews);
            if (wordsValidation == null) {
                return null;
            }
            response.push({
                "words": wordsValidation,
                "id_busines": business[i]["id"]
            });
        }
        return response;
    }
    async keywordsToBusiness(): Promise<Keywords[] | []> {
        let businessKeyword = new BusinessesKeywordsModel()
        let response = await businessKeyword.selectCount()
        let keywordsId = [];
        for (let keyword of response) {
            if (keyword.numberOfBusinesses > 1) {
                keywordsId.push({
                    keywords_id: keyword.keywords_id,
                    numberOfBusinesses: keyword.numberOfBusinesses,
                });
            }
        }
        return keywordsId;
    }
    async keywordsSearch(): Promise<WordAndBusinesId[] | boolean> {
        let keywordId = await this.keywordsToBusiness()
        if (keywordId.length < 1) {
            return false;
        }
        let businessKeyword = new BusinessesKeywordsModel()
        let data = [];
        for (let wordsId of keywordId) {
            let words = await businessKeyword.selectByKeywordId(wordsId);
            let businessId = await businessKeyword.selectBusinessId(wordsId)
            data.push({
                businessesNumber : wordsId.numberOfBusinesses,
                keyword: words[0]["words"],
                businessId: businessId
            })
        }
        return data;
    }
    async keywordsToCity(): Promise<FinalData[] | []> {
        let savedCities = await this.selectCityId()
        let selectByCityId = new BusinessModel();
        let selectByBusinessId = new BusinessesKeywordsModel()
        let finalData = []
        let cities = []
        let concat = "";
        for (let city of savedCities) {
            let savedBusinessesIds = await selectByCityId.selectByCityId(city)
            let keywords = []
            for (let businessId of savedBusinessesIds) {
                let savedKeywords = await selectByBusinessId.selectByBusinessId(businessId)
                for (let wordId of savedKeywords) {
                    let word:any = await selectByBusinessId.selectByKeywordId(wordId)
                    keywords.push(word.words)
                    if (word.length < 1) {
                        throw new Error("Keywords tabel it's empty. Make sure you have inserted some words.")
                    }
                }
            }
            finalData.push({
                city: city.city_name,
                words:keywords
             })
        }
        return finalData
    }
    async selectCityId():Promise<CityInterface[] | []> {
        let city = new CityModel();
        let savedCities = await city.select();
        if (savedCities.length < 1) {
            throw new Error("You don't have city inserted in the database.")
        }
        return savedCities;
    }
}