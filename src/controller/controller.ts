import { City } from "../modules/city";
import { Business } from "../modules/business";
import { businessesNumberValidator, citySchemaValidator } from "../validator/joi";
import express from "express";
import { Reviews } from "../models/reviews-model";
import { Review } from "../modules/review";
import { BusinessKeyword } from "../modules/business-keyword";
export class Controller {
    sequelize: any;
    router: any;
    constructor(router) {
        this.router = router;

        this.router.get('/cities-insert', async (req: express.Request, res: express.Response) => {
            try {
                await citySchemaValidator.validateAsync(req.query);
                let responseMessage = "This city it's already in tabel of cities.";
                let cityObject = new City(req.query.city.toLowerCase());
                let ifExist = await cityObject.insertCity(cityObject.city);
                if (ifExist) {
                    responseMessage = "All good. :) Your data it's succesfully inserted."
                }
                res.status(200).json({
                    message: responseMessage
                })
            } catch (error) {
                res.status(404).json({
                    message: error.message
                })
            }
            res.end();
        })
        this.router.post('/businesses-search', async (req: express.Request, res: express.Response) => {
            try {
                await citySchemaValidator.validateAsync(req.body)
                let responseMessage = "For this city data from businesses it's already avaible in database.";
                let cityAndRadius = new Business(req.body.city.toLowerCase(), req.body.radius);
                let businessesInformation = await cityAndRadius.insertBusinessExternal(cityAndRadius);
                if (businessesInformation.length > 1) {
                    responseMessage = "All good. :) Your data it's succesfully uploaded in Database."
                }
                res.status(200).json({
                    message: responseMessage,
                    data: businessesInformation
                })
            } catch (error) {
                res.status(404).json({
                    message: error.message
                })
                res.end();
            }
        })
        this.router.get('/reviews/:businessesNumber', async (req: express.Request, res: express.Response) => {
            try {
                await businessesNumberValidator.validateAsync(req.params)
                let responseMessage = "All good. :) Your data it's succesfully uploaded in Database.";
                let randomNumberOfBusinesses = new Reviews();
                let business = await randomNumberOfBusinesses.selectRandomIdBusinesses(req.params.businessesNumber)
                if (business.length < 1) {
                    responseMessage = "You don't have any businesses uploaded.";
                }
                let idsOfData = new BusinessKeyword();
                let wordsAndBusinesId = await idsOfData.yelpForBusiness(business)
                let review = new Review();
                let ids = await review.keywordsModul(wordsAndBusinesId);
                await idsOfData.businessKeyword(ids);
                res.status(200).json({
                    message: responseMessage
                })
            } catch (error) {
                res.status(404).json({
                    message: error.message
                })
            }
            res.end();
        })
        this.router.get('/stats/keywords', async ( req:express.Request, res: express.Response) => {
            try {
                let businessKeyword = new BusinessKeyword()
                let data = await businessKeyword.keywordsSearch()
                let responseMessage = "All good :). Here are your information:"
                if(!data){
                    responseMessage = ":( Somethig went wrong. Your data it's not availbe in Businesses_Keywords tabel."
                }
                res.status(200).json({
                    message: responseMessage, data
                })
            } catch (error) {
                res.status(404).json({
                    message: error.message
                })
            }
        })
        this.router.get('/stats/city', async (req:express.Request,res:express.Response) =>{
            try{
            let businessKeyword = new BusinessKeyword()
            let response = await businessKeyword.keywordsToCity()
            res.status(200).json({
                message: "All good", response
            })
        } catch(error){
            res.status(404).json({
                message: error.message
            })
        }
        })
    }
}


