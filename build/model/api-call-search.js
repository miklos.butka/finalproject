"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiCallForSearch = void 0;
const api_call_1 = require("../api-call/api-call");
class ApiCallForSearch extends api_call_1.ApiCall {
    constructor(city) {
        super();
        this.type = `location=${city}`;
    }
}
exports.ApiCallForSearch = ApiCallForSearch;
//# sourceMappingURL=api-call-search.js.map