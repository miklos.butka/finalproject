"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertCityInDB = void 0;
const sequelize_1 = require("sequelize");
const insert_city_1 = require("./cities/insert-city");
const select_city_1 = require("./cities/select-city");
class InsertCityInDB {
    constructor(city) {
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async verifyCity(city) {
        try {
            let select = new select_city_1.SelectCity();
            let insert = new insert_city_1.InsertCity();
            let response = await select.selectCity(city);
            if (response == false) {
                await insert.insertCity(city);
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.InsertCityInDB = InsertCityInDB;
//# sourceMappingURL=verify-city.js.map