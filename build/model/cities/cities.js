"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cities = void 0;
const sequelize_1 = require("sequelize");
class Cities {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertCity(city) {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${city.city}')`);
        await this.sequelize.query(queryInsert, { type: sequelize_1.QueryTypes.INSERT });
    }
    async selectCity(city) {
        try {
            let cityFromDB = (`SELECT id FROM cities WHERE city_name LIKE '${city.city}'`);
            let saveCityId = await this.sequelize.query(cityFromDB, { type: sequelize_1.QueryTypes.SELECT });
            if (saveCityId.length == 1) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.Cities = Cities;
//# sourceMappingURL=cities.js.map