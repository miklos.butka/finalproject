"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewModel = void 0;
const sequelize_1 = require("sequelize");
const keywords_1 = require("../model2/keywords");
class ReviewModel {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    //-------------------------------------// words filtration
    async filterKeywords(reviews) {
        try {
            let words = await this.extractKeywordsFromReview(reviews);
            if (!words) {
                return null;
            }
            let savedWords = [];
            for (let validWords of words) {
                if (validWords.length > 5 && validWords.length < 8) {
                    savedWords.push(validWords);
                }
            }
            return savedWords;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
    // -----------------------------------// words extraction
    async extractKeywordsFromReview(reviews) {
        try {
            let finalDataAfterFiltration = [];
            for (let reviewsObject of reviews) {
                let textFromReview = reviewsObject.text;
                const regexMatch = /[a-zA-Z'ÁÉÍÓÚáéíóúâêîôûàèìòùÇç]+/gm;
                let resultFromRegex = textFromReview.match(regexMatch);
                if (resultFromRegex == null) {
                    continue;
                }
                if (resultFromRegex.length >= 1) {
                    for (let words of resultFromRegex) {
                        let processedDataReviews = `${words.toLowerCase()}`;
                        finalDataAfterFiltration.push(processedDataReviews);
                    }
                }
            }
            return finalDataAfterFiltration;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
    async keywords(arrayOfArrayWords) {
        try {
            let arrayOfIds = [];
            let saveIds;
            if (arrayOfArrayWords) {
                for (let objectOfWords of arrayOfArrayWords) {
                    for (let word of objectOfWords["words"]) {
                        let selectIdKeyword = new keywords_1.Keywords();
                        let idOfWord = await selectIdKeyword.selectIdKeywords(word);
                        if (idOfWord == null) {
                            let finalWord = `("${word}")`;
                            let insertedWord = await selectIdKeyword.insertKeywords(finalWord);
                            saveIds = { "word_id": insertedWord[0], "id_business": objectOfWords["id_busines"] };
                        }
                        else {
                            saveIds = { "word_id": idOfWord[0].id, "id_business": objectOfWords["id_busines"] };
                        }
                        arrayOfIds.push(saveIds);
                    }
                }
            }
            return arrayOfIds;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.ReviewModel = ReviewModel;
//# sourceMappingURL=reviews-model.js.map