"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationDataFromDatabase = void 0;
const sequelize_1 = require("sequelize");
class ValidationDataFromDatabase {
    constructor(city, radius) {
        this.radius = radius;
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    } //returneaza un singur tip de date
    async idValidationFromCitiesTable() {
        let saveIDfromCitiesTabel = `SELECT id FROM cities WHERE city_name='${this.city.toLowerCase()}'`;
        let saveId = await this.sequelize.query(saveIDfromCitiesTabel, { type: sequelize_1.QueryTypes.SELECT });
        if (saveId.length == 0) {
            throw new Error("Make sure this city exist in cities table.");
        }
        return saveId[0].id;
    }
}
exports.ValidationDataFromDatabase = ValidationDataFromDatabase;
//# sourceMappingURL=search-model-validation.js.map