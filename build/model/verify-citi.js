"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertCityInDB = void 0;
const sequelize_1 = require("sequelize");
const cities_1 = require("./cities/cities");
class InsertCityInDB {
    constructor(city) {
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async verifyCity(city) {
        try {
            let verifyCity = new cities_1.Cities();
            let response = await verifyCity.selectCity(city);
            if (response == false) {
                await verifyCity.insertCity(city);
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.InsertCityInDB = InsertCityInDB;
//# sourceMappingURL=verify-citi.js.map