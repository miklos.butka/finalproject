"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationDataFromDatabase = void 0;
const sequelize_1 = require("sequelize");
class ValidationDataFromDatabase {
    constructor(city, radius) {
        this.radius = radius;
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    } //returneaza un singur tip de date
    async idValidationFromCitiesTable() {
        let saveIDfromCitiesTabel = `SELECT id FROM cities WHERE city_name='${this.city.toLowerCase()}'`;
        let saveId = await this.sequelize.query(saveIDfromCitiesTabel, { type: sequelize_1.QueryTypes.SELECT });
        if (saveId.length == 0) {
            return null;
        }
        return saveId[0].id;
    }
    async dataValidationFromBusinessesTable() {
        let saveCitiesId = await this.idValidationFromCitiesTable();
        if (saveCitiesId == null) {
            return null;
        }
        let saveAllDataFromBusinessTabel = `SELECT alias, name, review_count, rating, address, zip_code, country, display_address FROM businesses WHERE cities_id='${saveCitiesId}'`;
        let saveDataBusinesses = await this.sequelize.query(saveAllDataFromBusinessTabel, { type: sequelize_1.QueryTypes.SELECT });
        if (saveDataBusinesses.length > 1) {
            return 1;
        }
        else {
            return saveCitiesId;
        }
    }
}
exports.ValidationDataFromDatabase = ValidationDataFromDatabase;
//Am mutat verificariile de la db aici
//# sourceMappingURL=search-model-verification.js.map