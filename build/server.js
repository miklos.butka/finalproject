"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
//Server setup
const sequelize_1 = require("sequelize");
const controller_1 = require("./controller/controller");
let express = require('express');
class Server {
    constructor() {
        this.app = express();
        //check libraries of express
        this.app.use(express.json());
        let PORT = 3000;
        new controller_1.Controller(this.app);
        this.app.listen(PORT, () => {
            console.log(`Server running on port ${PORT}`);
        });
    }
    //Connection to db one single time
    async dbConnection() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map