"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiCallForReview = void 0;
const axios_1 = __importDefault(require("axios"));
const sequelize_1 = require("sequelize");
const reviews_model_1 = require("../modules/reviews-model");
class ApiCallForReview {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    ;
    async ApiCall(idAndBusinesIdObject) {
        try {
            let responseOfArray = [];
            for (let i = 0; i < idAndBusinesIdObject.length; i++) {
                let config = {
                    method: 'get',
                    url: `https://api.yelp.com/v3/businesses/${idAndBusinesIdObject[i]["id_business"]}/reviews`,
                    headers: {
                        Authorization: 'Bearer UV10z3kzuQgvg0CTUFnIPPXanr2tgILPfW0dJpM_GxJSb1kd92GC6B8RKeXcsj6q_yo4hO_U1BWANtSgex95D5VqyMcnXr8GI4iRS_CwB3FwsW2RmM3N5_OH63YaYHYx'
                    }
                };
                await (0, axios_1.default)(config)
                    .then(async function (response) {
                    let reviews = response.data.reviews;
                    let filtrationOfReviews = new reviews_model_1.ReviewModul();
                    let wordsValidation = await filtrationOfReviews.filterKeywords(reviews);
                    if (wordsValidation == null) {
                        return null;
                    }
                    responseOfArray.push({
                        "words": wordsValidation,
                        "id_busines": idAndBusinesIdObject[i]["id"]
                    });
                })
                    .catch(function (error) {
                    throw new Error(error.message);
                });
            }
            return responseOfArray;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.ApiCallForReview = ApiCallForReview;
//# sourceMappingURL=api-call-review.js.map