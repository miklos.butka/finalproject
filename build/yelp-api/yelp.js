"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Yelp = void 0;
const axios_1 = __importDefault(require("axios"));
class Yelp {
    constructor() {
        this.config = {
            headers: {
                Authorization: 'Bearer BAqXq0kGgwU11qcdUdMn8DaUysDlViqHCkSnXd1alWaURGlRHfSI74XaJM1HxuAm5z91Ld9ODN55ouJu-_yI2Y2OwET94eYaSKikmMYNiHC14EYraPuZsZ0svDDHYnYx'
            }
        };
    }
    async businessSearch(cityAndRadius) {
        let url = `https://api.yelp.com/v3/businesses/search?location=${cityAndRadius.city}&radius=${cityAndRadius.radius}`;
        let response = await axios_1.default.get(url, this.config);
        return response.data.businesses;
    }
    async reviews(business) {
        let url = `https://api.yelp.com/v3/businesses/${business["yelp_business_id"]}/reviews`;
        let response = await axios_1.default.get(url, this.config);
        return response.data.reviews;
    }
}
exports.Yelp = Yelp;
//# sourceMappingURL=yelp.js.map