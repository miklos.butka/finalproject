"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiCall = void 0;
let axios = require('axios');
class ApiCall {
    constructor() { }
    ;
    async apiCall(cityAndRadiusObject) {
        try {
            let config = {
                // method: 'get',
                headers: {
                    Authorization: 'Bearer BAqXq0kGgwU11qcdUdMn8DaUysDlViqHCkSnXd1alWaURGlRHfSI74XaJM1HxuAm5z91Ld9ODN55ouJu-_yI2Y2OwET94eYaSKikmMYNiHC14EYraPuZsZ0svDDHYnYx'
                }
            };
            let response = await axios.get(`"https://api.yelp.com/v3/businesses/search?location=${cityAndRadiusObject.city}&radius=${cityAndRadiusObject.radius}"`, config);
            if (response == null) {
                throw new Error("Data from API it's not avaible.");
            }
            return response.data.businesses;
            //     return await axios(config)
            //       .then(function (response) {
            //             let businesses = response.data.businesses;
            //             return businesses;
            //         })
            //         .catch(function (error) {
            //             throw new Error(error.message);
            //     });
        }
        catch (error) {
            error.message;
        }
    }
}
exports.ApiCall = ApiCall;
//# sourceMappingURL=api-call.js.map