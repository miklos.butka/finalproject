"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessesKeywords = void 0;
const sequelize_1 = require("sequelize");
class BusinessesKeywords {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertBusinesKeywords(ids) {
        try {
            if (typeof ids.word_id == 'number' && ids.word_id > 0 && typeof ids.id_business == 'number' && ids.id_business > 0)
                console.log(ids);
            for (let objectOfIds of ids) {
                console.log(objectOfIds);
                let insertToBusinesKeywords = `INSERT INTO businesses_keywords(businesses_id, keywords_id ) VALUES (${objectOfIds.id_business}, ${objectOfIds.word_id})`;
                await this.sequelize.query(insertToBusinesKeywords);
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.BusinessesKeywords = BusinessesKeywords;
//# sourceMappingURL=businesses-keywords.js.map