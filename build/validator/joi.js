"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.businessesNumberValidator = exports.citySchemaValidator = void 0;
const Joi = require("joi");
exports.citySchemaValidator = Joi.object({
    city: Joi.string()
        //$ asserts position at the end of the string
        .pattern(new RegExp(/^[a-zA-Z]*$/))
        .required(),
    radius: Joi.number()
        .integer()
        .min(10)
        .max(5000)
        .optional()
});
exports.businessesNumberValidator = Joi.object({
    businessesNumber: Joi.string()
        .pattern(new RegExp(/^[0-9]/))
        .required()
});
//# sourceMappingURL=joi.js.map