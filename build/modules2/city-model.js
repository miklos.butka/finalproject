"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CityModel = void 0;
const sequelize_1 = require("sequelize");
const cities_1 = require("../models/cities");
class CityModel {
    constructor(city) {
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async verifyCity(cityObject) {
        try {
            let select = new cities_1.Cities();
            let cityId = await select.selectCityId(cityObject);
            if (cityId > 0) {
                return true;
            }
            return false;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.CityModel = CityModel;
//# sourceMappingURL=city-model.js.map