"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchModel = void 0;
const sequelize_1 = require("sequelize");
const api_call_search_1 = require("../api-call/api-call-search");
const businesses_1 = require("../models/businesses");
class SearchModel {
    constructor(city, radius) {
        this.city = city;
        this.radius = radius;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async businessesModel(cityAndRadiusObject) {
        try {
            let API = new api_call_search_1.ApiCallForSearch(this.city, this.radius);
            let businesses = await API.apiCall();
            // if (!businesses) {
            //     throw new Error("Check you API request");
            // }
            let ifExistAlready = new businesses_1.Businesses();
            let cityId = await ifExistAlready.selectBusinesses(cityAndRadiusObject);
            if (cityId == false) {
                return false;
            }
            let bulkedData = '';
            for (let business of businesses) {
                if (business.rating >= 3 && business.rating <= 5 && business.review_count >= 10) {
                    let processedBusinessData = `(${cityId},"${business.id}","${business.alias}","${business.name}",${business.review_count},${business.rating},"${business.location.address1}", ${business.location.zip_code}, "${business.location.country}","${business.location.display_address}")`;
                    bulkedData = bulkedData.concat(processedBusinessData, ',');
                }
            }
            bulkedData = bulkedData.slice(0, -1);
            return bulkedData;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.SearchModel = SearchModel;
//# sourceMappingURL=businesses-model.js.map