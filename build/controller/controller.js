"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
const city_1 = require("../modules/city");
const business_1 = require("../modules/business");
const joi_1 = require("../validator/joi");
const reviews_model_1 = require("../models/reviews-model");
const review_1 = require("../modules/review");
const business_keyword_1 = require("../modules/business-keyword");
class Controller {
    constructor(router) {
        this.router = router;
        this.router.get('/cities-insert', async (req, res) => {
            try {
                await joi_1.citySchemaValidator.validateAsync(req.query);
                let responseMessage = "This city it's already in tabel of cities.";
                let cityObject = new city_1.City(req.query.city.toLowerCase());
                let ifExist = await cityObject.insertCity(cityObject.city);
                if (ifExist) {
                    responseMessage = "All good. :) Your data it's succesfully inserted.";
                }
                res.status(200).json({
                    message: responseMessage
                });
            }
            catch (error) {
                res.status(404).json({
                    message: error.message
                });
            }
            res.end();
        });
        this.router.post('/businesses-search', async (req, res) => {
            try {
                await joi_1.citySchemaValidator.validateAsync(req.body);
                let responseMessage = "For this city data from businesses it's already avaible in database.";
                let cityAndRadius = new business_1.Business(req.body.city.toLowerCase(), req.body.radius);
                let businessesInformation = await cityAndRadius.insertBusinessExternal(cityAndRadius);
                if (businessesInformation.length > 1) {
                    responseMessage = "All good. :) Your data it's succesfully uploaded in Database.";
                }
                res.status(200).json({
                    message: responseMessage,
                    data: businessesInformation
                });
            }
            catch (error) {
                res.status(404).json({
                    message: error.message
                });
                res.end();
            }
        });
        this.router.get('/reviews/:businessesNumber', async (req, res) => {
            try {
                await joi_1.businessesNumberValidator.validateAsync(req.params);
                let responseMessage = "All good. :) Your data it's succesfully uploaded in Database.";
                let randomNumberOfBusinesses = new reviews_model_1.Reviews();
                let business = await randomNumberOfBusinesses.selectRandomIdBusinesses(req.params.businessesNumber);
                if (business.length < 1) {
                    responseMessage = "You don't have any businesses uploaded.";
                }
                let idsOfData = new business_keyword_1.BusinessKeyword();
                let wordsAndBusinesId = await idsOfData.yelpForBusiness(business);
                let review = new review_1.Review();
                let ids = await review.keywordsModul(wordsAndBusinesId);
                await idsOfData.businessKeyword(ids);
                res.status(200).json({
                    message: responseMessage
                });
            }
            catch (error) {
                res.status(404).json({
                    message: error.message
                });
            }
            res.end();
        });
        this.router.get('/stats/keywords', async (req, res) => {
            try {
                let businessKeyword = new business_keyword_1.BusinessKeyword();
                let data = await businessKeyword.keywordsSearch();
                let responseMessage = "All good :). Here are your information:";
                if (!data) {
                    responseMessage = ":( Somethig went wrong. Your data it's not availbe in Businesses_Keywords tabel.";
                }
                res.status(200).json({
                    message: responseMessage, data
                });
            }
            catch (error) {
                res.status(404).json({
                    message: error.message
                });
            }
        });
        this.router.get('/stats/city', async (req, res) => {
            try {
                let businessKeyword = new business_keyword_1.BusinessKeyword();
                let response = await businessKeyword.keywordsToCity();
                res.status(200).json({
                    message: "All good", response
                });
            }
            catch (error) {
                res.status(404).json({
                    message: error.message
                });
            }
        });
    }
}
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map