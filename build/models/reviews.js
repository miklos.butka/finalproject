"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reviews = void 0;
const sequelize_1 = require("sequelize");
class Reviews {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async selectRandomIdBusinesses(randomNumberOfBusinesses) {
        let idForBusiness = (`SELECT id, id_business FROM businesses ORDER BY RAND() LIMIT ${randomNumberOfBusinesses}`);
        let saveOfIds = await this.sequelize.query(idForBusiness, { type: sequelize_1.QueryTypes.SELECT });
        return saveOfIds;
    }
}
exports.Reviews = Reviews;
//# sourceMappingURL=reviews.js.map