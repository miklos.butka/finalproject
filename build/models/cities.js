"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CitiesModel = void 0;
const sequelize_1 = require("sequelize");
class CitiesModel {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insert(cityObject) {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${cityObject.city}')`);
        await this.sequelize.query(queryInsert, { type: sequelize_1.QueryTypes.INSERT });
    }
    async select(cityObject) {
        let cityId = (`SELECT id FROM cities WHERE city_name LIKE '${cityObject.city}'`);
        let savedCityId = await this.sequelize.query(cityId, { type: sequelize_1.QueryTypes.SELECT });
        return savedCityId;
    }
}
exports.CitiesModel = CitiesModel;
//# sourceMappingURL=cities.js.map