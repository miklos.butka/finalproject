"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Keywords = void 0;
const sequelize_1 = require("sequelize");
class Keywords {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async select(word) {
        let verifyId = `SELECT id FROM keywords WHERE words="${word}"`;
        let idFromKeyword = await this.sequelize.query(verifyId, { type: sequelize_1.QueryTypes.SELECT });
        return idFromKeyword;
    }
    async insert(finalWord) {
        let insertToKeywordsTable = `INSERT INTO keywords(words) VALUES ${finalWord}`;
        let insertedWord = await this.sequelize.query(insertToKeywordsTable);
        return insertedWord;
    }
}
exports.Keywords = Keywords;
//# sourceMappingURL=keywords.js.map