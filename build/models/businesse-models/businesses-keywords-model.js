"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessesKeywordsModel = void 0;
const sequelize_1 = require("sequelize");
class BusinessesKeywordsModel {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertBusinesKeywords(objectOfIds) {
        let insertToBusinesKeywords = `INSERT INTO businesses_keywords(businesses_id, keywords_id ) VALUES (${objectOfIds.id_business}, ${objectOfIds.word_id})`;
        await this.sequelize.query(insertToBusinesKeywords);
    }
}
exports.BusinessesKeywordsModel = BusinessesKeywordsModel;
//# sourceMappingURL=businesses-keywords-model.js.map