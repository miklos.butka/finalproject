"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectBusinesses = void 0;
const sequelize_1 = require("sequelize");
const select_city_1 = require("../cities/select-city");
class SelectBusinesses {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async selectBusinesses(cityAndRadius) {
        let selectCityId = new select_city_1.SelectCity();
        let cityId = await selectCityId.selectCityIdForBusinesses(cityAndRadius);
        let dataFromBusinesses = `SELECT alias, name, review_count, rating, address, zip_code, country, display_address FROM businesses WHERE cities_id='${cityId}'`;
        let ifExistData = await this.sequelize.query(dataFromBusinesses, { type: sequelize_1.QueryTypes.SELECT });
        if (ifExistData.length > 1) {
            throw new Error("This data already exist in businesses table.");
        }
        else {
            return cityId;
        }
    }
}
exports.SelectBusinesses = SelectBusinesses;
//# sourceMappingURL=select-businesses.js.map