"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Businesses = void 0;
const sequelize_1 = require("sequelize");
const cities_1 = require("../cities/cities");
class Businesses {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertBusinesses(businessesInformation) {
        let insertToBusinessTable = `INSERT INTO businesses(cities_id, id_business,alias, name, review_count, rating, address, zip_code, country, display_address) VALUES ${businessesInformation}`;
        await this.sequelize.query(insertToBusinessTable);
    }
    async selectBusinesses(cityAndRadiusObject) {
        let selectCityId = new cities_1.Cities();
        let cityId = await selectCityId.selectCityId(cityAndRadiusObject);
        if (!cityId) {
            throw new Error("You don't have this city inserted in table of Cities.Please insert the city and try again");
        }
        let dataFromBusinesses = `SELECT alias, name, review_count, rating, address, zip_code, country, display_address FROM businesses WHERE cities_id='${cityId}'`;
        let ifExistData = await this.sequelize.query(dataFromBusinesses, { type: sequelize_1.QueryTypes.SELECT });
        if (ifExistData.length > 1) {
            return false;
        }
        return cityId;
    }
}
exports.Businesses = Businesses;
//# sourceMappingURL=businesses.js.map