"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertBusinesses = void 0;
const sequelize_1 = require("sequelize");
class InsertBusinesses {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertBusinesses(bulkedData) {
        let insertToBusinessTable = `INSERT INTO businesses(cities_id, id_business,alias, name, review_count, rating, address, zip_code, country, display_address) VALUES ${bulkedData}`;
        await this.sequelize.query(insertToBusinessTable);
    }
}
exports.InsertBusinesses = InsertBusinesses;
//# sourceMappingURL=insert-businesses.js.map