"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchBusinesModul = void 0;
const sequelize_1 = require("sequelize");
const api_call_1 = require("../api-call/api-call");
const businesses_model_1 = require("../models/businesses-model");
const cities_model_1 = require("../models/cities-model");
class SearchBusinesModul {
    constructor(city, radius) {
        this.city = city;
        this.radius = radius;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async businessesModul(cityAndRadiusObject) {
        try {
            let API = new api_call_1.ApiCall();
            let businesses = await API.apiCall(cityAndRadiusObject);
            let selectCityId = new cities_model_1.CitiesModel();
            let cityId = await selectCityId.select(cityAndRadiusObject);
            if (cityId.length < 1) {
                throw new Error("This city don't exist in tabel of Cities. Make sure to insert this city in the database first time to take to information about the businnesses.");
            }
            let businessesDataSelection = new businesses_model_1.BusinessesModel();
            let ifDataExist = await businessesDataSelection.select(cityId);
            if (ifDataExist.length > 0) {
                return false;
            }
            let bulkedData = '';
            for (let business of businesses) {
                if (business.rating >= 3 && business.rating <= 5 && business.review_count >= 10) {
                    let processedBusinessData = `(${cityId[0]["id"]},"${business.id}","${business.alias}","${business.name}",${business.review_count},${business.rating},"${business.location.address1}", ${business.location.zip_code}, "${business.location.country}","${business.location.display_address}")`;
                    bulkedData = bulkedData.concat(processedBusinessData, ',');
                }
            }
            bulkedData = bulkedData.slice(0, -1);
            return bulkedData;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.SearchBusinesModul = SearchBusinesModul;
//# sourceMappingURL=businesses-model.js.map