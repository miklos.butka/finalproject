"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cities = void 0;
const sequelize_1 = require("sequelize");
class Cities {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async selectCity(cityObject) {
        try {
            let cityFromDB = (`SELECT id FROM cities WHERE city_name LIKE '${cityObject.city}'`);
            let saveCityId = await this.sequelize.query(cityFromDB, { type: sequelize_1.QueryTypes.SELECT });
            if (saveCityId.length == 1) {
                throw new Error("This city it's already avaible in your database.");
            }
            else {
                return false;
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
    async selectCityIdForBusinesses(cityAndRadius) {
        let saveIDfromCitiesTabel = `SELECT id FROM cities WHERE city_name='${cityAndRadius.city}'`;
        let saveId = await this.sequelize.query(saveIDfromCitiesTabel, { type: sequelize_1.QueryTypes.SELECT });
        if (saveId.length == 0) {
            throw new Error("Make sure this city exist in cities table.");
        }
        else if (saveId == undefined) {
            throw new Error(":( Something went wrong from id selection.");
        }
        return saveId[0].id;
    }
}
exports.Cities = Cities;
//# sourceMappingURL=select-city.js.map