"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Business = void 0;
const sequelize_1 = require("sequelize");
const businesses_model_1 = require("../models/businesses-model");
const cities_model_1 = require("../models/cities-model");
const yelp_1 = require("../yelp-api/yelp");
class Business {
    constructor(city, radius) {
        this.city = city;
        this.radius = radius;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertBusinessExternal(cityObject) {
        try {
            let cityModel = new cities_model_1.CityModel();
            let cityId = await cityModel.selectByCityName(cityObject.city);
            if (cityId.length < 1) {
                throw new Error("This city don't exist in tabel of Cities. Make sure to insert this city in the database first time to take to information about the businnesses.");
            }
            let yelp = new yelp_1.Yelp();
            let businesses = await yelp.businessSearch(cityObject);
            if (businesses == null) {
                throw new Error("Data from YELP it's not avaible.");
            }
            let businessesData = [];
            let businessModel = new businesses_model_1.BusinessModel();
            if (businesses.length > 0) {
                for (let business of businesses) {
                    if (business.rating >= 3 && business.rating <= 5 && business.review_count >= 10) {
                        let businessData = await businessModel.selectByYelpBusinessId(business.id);
                        if (businessData.length == 0) {
                            let processedBusinessData = `(${cityId[0]["id"]},"${business.id}","${business.alias}","${business.name}",${business.review_count},${business.rating},"${business.location.address1}", ${business.location.zip_code}, "${business.location.country}","${business.location.display_address}")`;
                            businessesData.push({
                                cities_id: cityId[0]["id"],
                                yelp_business_id: business.id,
                                alias: business.alias,
                                name: business.name,
                                review_count: business.review_count,
                                rating: business.rating,
                                addres: business.location.address1,
                                zip_code: business.location.zip_code,
                                country: business.location.country,
                                display_addres: business.location.display_address,
                            });
                            await businessModel.insert(processedBusinessData);
                        }
                    }
                }
                return businessesData;
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.Business = Business;
//# sourceMappingURL=business.js.map