"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CitiesModul = void 0;
const sequelize_1 = require("sequelize");
const cities_model_1 = require("../models/cities-model");
class CitiesModul {
    constructor(city) {
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async verifyCity(cityObject) {
        try {
            let select = new cities_model_1.CitiesModel();
            let cityId = await select.select(cityObject);
            if (cityId.length > 0) {
                return true;
            }
            return false;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.CitiesModul = CitiesModul;
//# sourceMappingURL=city-model.js.map