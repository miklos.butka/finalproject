"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessKeyword = void 0;
const businesses_keywords_model_1 = require("../models/businesses-keywords-model");
const businesses_model_1 = require("../models/businesses-model");
const cities_model_1 = require("../models/cities-model");
const yelp_1 = require("../yelp-api/yelp");
const review_1 = require("./review");
class BusinessKeyword {
    constructor() { }
    async businessKeyword(ids) {
        let insert = new businesses_keywords_model_1.BusinessesKeywordsModel();
        for (let objectOfIds of ids) {
            let wordAndBusinessId = await insert.selectByWordIdAndBusinessID(objectOfIds);
            if (typeof objectOfIds.word_id == 'number' && objectOfIds.word_id > 0 && typeof objectOfIds.id_business == 'number' && objectOfIds.id_business > 0) {
                if (wordAndBusinessId.length < 1) {
                    await insert.insertBusinesKeywords(objectOfIds);
                }
            }
        }
    }
    async yelpForBusiness(business) {
        let response = [];
        for (let i = 0; i < business.length; i++) {
            let yelpApi = new yelp_1.Yelp();
            let reviews = await yelpApi.reviews(business[i]);
            if (reviews == null) {
                throw new Error("Data from YELP unavaible.");
            }
            let filtrationOfReviews = new review_1.Review();
            let wordsValidation = await filtrationOfReviews.filterKeywords(reviews);
            if (wordsValidation == null) {
                return null;
            }
            response.push({
                "words": wordsValidation,
                "id_busines": business[i]["id"]
            });
        }
        return response;
    }
    async keywordsToBusiness() {
        let businessKeyword = new businesses_keywords_model_1.BusinessesKeywordsModel();
        let response = await businessKeyword.selectCount();
        let keywordsId = [];
        for (let keyword of response) {
            if (keyword.numberOfBusinesses > 1) {
                keywordsId.push({
                    keywords_id: keyword.keywords_id,
                    numberOfBusinesses: keyword.numberOfBusinesses,
                });
            }
        }
        return keywordsId;
    }
    async keywordsSearch() {
        let keywordId = await this.keywordsToBusiness();
        if (keywordId.length < 1) {
            return false;
        }
        let businessKeyword = new businesses_keywords_model_1.BusinessesKeywordsModel();
        let data = [];
        for (let wordsId of keywordId) {
            let words = await businessKeyword.selectByKeywordId(wordsId);
            let businessId = await businessKeyword.selectBusinessId(wordsId);
            data.push({
                businessesNumber: wordsId.numberOfBusinesses,
                keyword: words[0]["words"],
                businessId: businessId
            });
        }
        return data;
    }
    async keywordsToCity() {
        let savedCities = await this.selectCityId();
        let selectByCityId = new businesses_model_1.BusinessModel();
        let selectByBusinessId = new businesses_keywords_model_1.BusinessesKeywordsModel();
        let finalData = [];
        let cities = [];
        let concat = "";
        for (let city of savedCities) {
            let savedBusinessesIds = await selectByCityId.selectByCityId(city);
            let keywords = [];
            for (let businessId of savedBusinessesIds) {
                let savedKeywords = await selectByBusinessId.selectByBusinessId(businessId);
                for (let wordId of savedKeywords) {
                    let word = await selectByBusinessId.selectByKeywordId(wordId);
                    keywords.push(word.words);
                    if (word.length < 1) {
                        throw new Error("Keywords tabel it's empty. Make sure you have inserted some words.");
                    }
                }
            }
            finalData.push({
                city: city.city_name,
                words: keywords
            });
        }
        return finalData;
    }
    async selectCityId() {
        let city = new cities_model_1.CityModel();
        let savedCities = await city.select();
        if (savedCities.length < 1) {
            throw new Error("You don't have city inserted in the database.");
        }
        return savedCities;
    }
}
exports.BusinessKeyword = BusinessKeyword;
//# sourceMappingURL=business-keyword.js.map