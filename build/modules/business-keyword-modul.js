"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessKeywordModul = void 0;
const businesses_keywords_model_1 = require("../models/businesses-keywords-model");
const yelp_1 = require("../yelp-api/yelp");
const review_1 = require("./review");
class BusinessKeywordModul {
    constructor() { }
    async businessKeyword(ids) {
        for (let objectOfIds of ids) {
            if (typeof objectOfIds.word_id == 'number' && objectOfIds.word_id > 0 && typeof objectOfIds.id_business == 'number' && objectOfIds.id_business > 0) {
                let insert = new businesses_keywords_model_1.BusinessesKeywordsModel();
                await insert.insertBusinesKeywords(objectOfIds);
            }
        }
    }
    async yelpLoopForBusiness(businesObject) {
        let responseOfArray = [];
        for (let i = 0; i < businesObject.length; i++) {
            let yelpApi = new yelp_1.Yelp();
            let reviewsObject = await yelpApi.reviews(businesObject[i]);
            let filtrationOfReviews = new review_1.ReviewModul();
            let wordsValidation = await filtrationOfReviews.filterKeywords(reviewsObject);
            if (wordsValidation == null) {
                return null;
            }
            responseOfArray.push({
                "words": wordsValidation,
                "id_busines": businesObject[i]["id"]
            });
        }
        return responseOfArray;
    }
}
exports.BusinessKeywordModul = BusinessKeywordModul;
//# sourceMappingURL=business-keyword-modul.js.map