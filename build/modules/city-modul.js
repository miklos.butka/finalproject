"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.City = void 0;
const sequelize_1 = require("sequelize");
const cities_model_1 = require("../models/cities-model");
class City {
    constructor(city) {
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async verifyCity(cityObject) {
        try {
            let cityModel = new cities_model_1.CitiesModel();
            let cityId = await cityModel.select(cityObject);
            if (cityId.length < 1) {
                await cityModel.insert(cityId[0]["id"]);
            }
            return false;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.City = City;
//# sourceMappingURL=city-modul.js.map