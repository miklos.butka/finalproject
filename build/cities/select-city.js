"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectCity = void 0;
const sequelize_1 = require("sequelize");
class SelectCity {
    async selectCity(city) {
        try {
            let cityFromDB = (`SELECT id FROM cities WHERE city_name LIKE '${city}'`);
            let saveCityId = await this.sequelize.query(cityFromDB, { type: sequelize_1.QueryTypes.SELECT });
            if (saveCityId.length == 1) {
                throw new Error("This city it's already avaible in your database.");
            }
            else {
                return false;
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.SelectCity = SelectCity;
//# sourceMappingURL=select-city.js.map