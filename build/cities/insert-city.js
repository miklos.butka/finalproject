"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertCity = void 0;
const sequelize_1 = require("sequelize");
class InsertCity {
    constructor(city) {
        this.city = city;
    }
    async insertCity() {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${this.city.toLowerCase()}')`);
        await this.sequelize.query(queryInsert, { type: sequelize_1.QueryTypes.INSERT });
    }
}
exports.InsertCity = InsertCity;
//# sourceMappingURL=insert-city.js.map